# SMD5050 IoT-Pi Module
Simple module to control SMD5050 LED Strips

## Installation

1. Put `smd5050.py` in `iotpi/modules/` folder
2. Import the module in `iotpi/init.py`:
    ```python
    import iotpi.modules.smd5050 as smd5050
    self.reader = smd5050.module(12,16,17) #Set MOSFET pins: r, g, b
    ```