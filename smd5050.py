import pigpio
import time

class module():
    def set(self, r, g, b):
        self.gpio.set_PWM_dutycycle(self.rPin, r)
        self.rState = r
        self.gpio.set_PWM_dutycycle(self.gPin, g)
        self.gState = g
        self.gpio.set_PWM_dutycycle(self.bPin, b)
        self.bState = b

    def fade(self, r, g, b, speed):        
        def genNext(state, target):
            if (state < target):
                return state + 1
            elif (state > target):
                return state - 1
            else:
                return state

        while (self.rState != r or self.bState != b or self.bState != b):
            rNext = genNext(self.rState, r)
            gNext = genNext(self.gState, g)
            bNext = genNext(self.bState, b)
            self.set(rNext, gNext, bNext)
            time.sleep(0.1 / speed)

    def __init__(self, rPin, gPin, bPin):
        self.rPin = rPin
        self.gPin = gPin
        self.bPin = bPin

        self.gpio = pigpio.pi()

        self.set(0, 0, 0)
    def __del__(self):
        self.gpio.stop()